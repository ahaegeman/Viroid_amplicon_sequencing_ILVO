#!/bin/bash
# (c) Annelies Haegeman 2018
# annelies.haegeman@ilvo.vlaanderen.be
# This bash script can be used to process a set of amplicon sequencing samples, in this case specifically for viroid amplicons.
# The raw data (F and R reads) of all samples (already demultiplexed) should be placed in the same folder.

#Specify here the folder where your samples are:
INPUTDIR=/home/genomics/ahaegeman/Virus/viroid_amplicon_sequencing/Run2/1_Demultiplexing

#Specify here the output directory where you want to put the results (should be an existing directory)
OUTPUTDIR=/home/genomics/ahaegeman/Virus/viroid_amplicon_sequencing/Run2/2_Preprocessing

#Specify here the folder where your scripts are (for example R script for length distribution)
SCRIPTSDIR=/home/genomics/ahaegeman/GitLab/Viroid_amplicon_sequencing/Preprocessing

#Specify the minimum number of times a sequence needs to be seen (over all samples) to be kept in the final output table of haplotypes
MINCOUNT=1000

date
echo

#change directory to specified folder
cd $INPUTDIR

#create several new directories to put the output after each step
mkdir $OUTPUTDIR/1_merged
mkdir $OUTPUTDIR/2_trimmed
mkdir $OUTPUTDIR/3_quality_filtered
mkdir $OUTPUTDIR/4_length_distribution
mkdir $OUTPUTDIR/5_length_filtered
mkdir $OUTPUTDIR/6_revcomp
mkdir $OUTPUTDIR/7_haplotype_counts
mkdir $OUTPUTDIR/8_overall_haplotype_counts


# Define the variable FILES that contains all the forward data sets (here only forward, otherwise you will do everything in duplicate!)
# When you make a variable, you can not use spaces! Otherwise you get an error.
FILES=( *_F.fq )

# Initiate file with number of reads
#echo -e "sample""\t""raw read pairs""\t""merged read pairs""\t""trimmed sequences""\t""quality filtered sequences""\t""long quality filtered sequences" > $OUTPUTDIR/Number_of_reads.txt


#Loop over all files and do all the commands
for f in "${FILES[@]}" 
do 
	#Define the variable SAMPLE who contains the basename where the extension is removed (-1.fastq.gz)
	SAMPLE=`basename $f _F.fq`

	echo
	echo "PROCESSING sample $SAMPLE "
	echo

	#MERGE F AND R READ
	echo "merging reads for $SAMPLE...."
	pear -f "$SAMPLE"_F.fq -r "$SAMPLE"_R.fq -o $OUTPUTDIR/1_merged/"$SAMPLE" -p 1.0 -v 20 -n 150 -m 400 -y 100G -j 8 -u 1
	echo "done merging reads for $SAMPLE."

	#PRIMER REMOVAL
	echo "removing primers for $SAMPLE...."
	#remove first primer using cutadapt. forward primer: TGCAGGGTCAGGTGTGAACCAC
	cutadapt -g TGCAGGGTCAGGTGTGAACCAC --discard-untrimmed -e 0.15 -o $OUTPUTDIR/2_trimmed/"$SAMPLE"-trimmed-5prime.fq $OUTPUTDIR/1_merged/"$SAMPLE".assembled.fastq
	#remove second primer using cutadapt. reverse primer: GCCATGCAAAGRAAAAAGAAYGGG
	cutadapt -a CCCRTTCTTTTTYCTTTGCATGGC --discard-untrimmed -e 0.15 -m 150 -M 400 -o $OUTPUTDIR/2_trimmed/"$SAMPLE"-fullytrimmed.fq $OUTPUTDIR/2_trimmed/"$SAMPLE"-trimmed-5prime.fq
	#remove temporary file with only one side trimmed
	rm $OUTPUTDIR/2_trimmed/"$SAMPLE"-trimmed-5prime.fq
	echo "done removing primers for $SAMPLE."

	#QUALITY STATISTICS AND FILTERING
	echo "calculating quality statistics and filtering $SAMPLE...."
	vsearch -fastq_stats $OUTPUTDIR/2_trimmed/"$SAMPLE"-fullytrimmed.fq -log $OUTPUTDIR/3_quality_filtered/"$SAMPLE"_fastqstats.log
	vsearch -fastq_filter $OUTPUTDIR/2_trimmed/"$SAMPLE"-fullytrimmed.fq -fastaout $OUTPUTDIR/3_quality_filtered/"$SAMPLE"_merged_filtered.fa -fastq_maxee 0.25
	echo "done calculating quality statistics and trimming $SAMPLE."
	
	#PLOT SEQUENCE LENGTH DISTRIBUTION
	echo "plotting length distribution for $SAMPLE...."
	cat $OUTPUTDIR/3_quality_filtered/"$SAMPLE"_merged_filtered.fa | awk '{if(NR%2==0) print length($1)}' | sort -n | uniq -c > $OUTPUTDIR/4_length_distribution/"$SAMPLE"_seq_lengths.txt
	Rscript $SCRIPTSDIR/length_distribution.R $OUTPUTDIR/4_length_distribution "$SAMPLE"_seq_lengths.txt "$SAMPLE"_seq_lengths.pdf
	echo "done plotting length distribution for $SAMPLE."
	
	#FILTER BASED ON SEQUENCE LENGTH (minimum 300 bp)
	echo "filtering sequences smaller than 300 bp and larger than 350 bp for $SAMPLE...."
	cutadapt -m 300 -M 350 -o $OUTPUTDIR/5_length_filtered/"$SAMPLE"_merged_filtered_long.fa $OUTPUTDIR/3_quality_filtered/"$SAMPLE"_merged_filtered.fa 
	echo "done filtering sequences smaller than 300 bp and larger than 350 bp for $SAMPLE."
	
	#TAKE REVERSE COMPLEMENT
	echo "taking reverse complement of $SAMPLE...."
	fastx_reverse_complement -i $OUTPUTDIR/5_length_filtered/"$SAMPLE"_merged_filtered_long.fa -o $OUTPUTDIR/6_revcomp/"$SAMPLE"_merged_filtered_long_RC.fa
	echo "done taking reverse complement of $SAMPLE."
	
	#COUNT OCCURRENCE OF EACH HAPLOTYPE
	echo "counting haplotypes of $SAMPLE...."
	obiuniq --raw-fasta --nuc --without-progress-bar $OUTPUTDIR/6_revcomp/"$SAMPLE"_merged_filtered_long_RC.fa > $OUTPUTDIR/7_haplotype_counts/"$SAMPLE"_derep.fa
	obisort --without-progress-bar -k count -r $OUTPUTDIR/7_haplotype_counts/"$SAMPLE"_derep.fa > $OUTPUTDIR/7_haplotype_counts/"$SAMPLE"_derep_sort.fa
	rm $OUTPUTDIR/7_haplotype_counts/"$SAMPLE"_derep.fa
	obistat --without-progress-bar -c count $OUTPUTDIR/7_haplotype_counts/"$SAMPLE"_derep_sort.fa | sort -nrk1 > $OUTPUTDIR/7_haplotype_counts/"$SAMPLE"_counts.txt
	echo "done counting haplotypes of $SAMPLE."	
	
	echo "renaming sequences from $SAMPLE...."
	perl $SCRIPTSDIR/rename_seqs_obitools.pl $OUTPUTDIR/6_revcomp/"$SAMPLE"_merged_filtered_long_RC.fa $SAMPLE $OUTPUTDIR/8_overall_haplotype_counts/"$SAMPLE"_merged_filtered_long_RC_renamed.fa
	echo "done renaming sequences from $SAMPLE."
	
	#create a new variable that is a string which contains all sample names ($SAMPLE) with extension _merged_filtered_renamed.fa
	ALLSAMPLES+=""$OUTPUTDIR"/8_overall_haplotype_counts/"$SAMPLE"_merged_filtered_long_RC_renamed.fa "
	
	#COUNT NUMBER OF READS
	echo "counting number of reads for $SAMPLE...."
	RAWREADS=`cat "$SAMPLE"_F.fq | wc -l |awk '{print $1/4}'`
	MERGEDREADS=`cat "$OUTPUTDIR"/1_merged/"$SAMPLE".assembled.fastq | wc -l |awk '{print $1/4}'`
	TRIMMEDREADS=`cat "$OUTPUTDIR"/2_trimmed/"$SAMPLE"-fullytrimmed.fq | wc -l |awk '{print $1/4}'`
	FILTEREDREADS=`cat "$OUTPUTDIR"/3_quality_filtered/"$SAMPLE"_merged_filtered.fa | wc -l |awk '{print $1/2}'`
	FILTEREDREADSLONG=`cat "$OUTPUTDIR"/5_length_filtered/"$SAMPLE"_merged_filtered_long.fa | wc -l |awk '{print $1/2}'`
	echo -e "$SAMPLE""\t""$RAWREADS""\t""$MERGEDREADS""\t""$TRIMMEDREADS""\t""$FILTEREDREADS""\t""$FILTEREDREADSLONG" >> $OUTPUTDIR/Number_of_reads.txt
	echo "done counting number of reads for $SAMPLE."
	
done

#HAPLOTYPE CALLING FOR ALL SAMPLES COMBINED
#from now on, all samples will be concatenated, and can be processed in single commands only (the loop is not necessary anymore).

#CONCATENATE ALL SEQUENCES
echo
echo "concatenating all samples...."
cat $ALLSAMPLES > $OUTPUTDIR/8_overall_haplotype_counts/all_samples.fa
rm $OUTPUTDIR/8_overall_haplotype_counts/*_merged_filtered_long_RC_renamed.fa
echo "done concatenating all samples."
echo

#DEREPLICATION
echo
echo "dereplicating sequences of all samples...."
obiuniq --without-progress-bar -m sample $OUTPUTDIR/8_overall_haplotype_counts/all_samples.fa > $OUTPUTDIR/8_overall_haplotype_counts/all_derep.fa
#count number of occurrences of each sequence
obistat --without-progress-bar -c count $OUTPUTDIR/8_overall_haplotype_counts/all_derep.fa | sort -nk1 > $OUTPUTDIR/8_overall_haplotype_counts/all_derep_unique_sequence_counts.txt
echo "done dereplicating sequences of all samples."
echo

#FILTERING
echo
echo "filtering sequences of all samples...."
#keep only sequences that occur > MINCOUNT times
obigrep --without-progress-bar -p "count>=$MINCOUNT" $OUTPUTDIR/8_overall_haplotype_counts/all_derep.fa > $OUTPUTDIR/8_overall_haplotype_counts/all_derep_mincount"$MINCOUNT".fa
#identify PCR and/or sequencing errors => NOT USED IN THIS PIPELINE
#obiclean --without-progress-bar -s merged_sample -r 0.05 -H -d 1 $OUTPUTDIR/8_overall_haplotype_counts/all_derep_mincount"$MINCOUNT".fa > $OUTPUTDIR/8_overall_haplotype_counts/all_derep_mincount"$MINCOUNT"_cleaned.fa
echo "done filtering sequences of all samples."

#SORT AND OUTPUT
#sort the sequences according to abundance
obisort --without-progress-bar -k count -r $OUTPUTDIR/8_overall_haplotype_counts/all_derep_mincount"$MINCOUNT".fa > $OUTPUTDIR/8_overall_haplotype_counts/all_derep_mincount"$MINCOUNT"_sort.fa
#generate a tab delimited output table
obitab --without-progress-bar -o $OUTPUTDIR/8_overall_haplotype_counts/all_derep_mincount"$MINCOUNT"_sort.fa > $OUTPUTDIR/8_overall_haplotype_counts/all_derep_mincount"$MINCOUNT"_sort_final_table.txt
