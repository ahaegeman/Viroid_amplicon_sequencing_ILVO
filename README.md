# Viroid_amplicon_sequencing_ILVO

## Introduction
This pipeline is a customized pipeline built to analyse amplicon sequencing data from viroids resulting from the library preparation method described below.

#### Library preparation
The library preparation method consists of a PCR step with custom primers to amplify the region of interest. In this case we use the primerset "CLVd-infect" (c-CLVd-infect and h-CLVd-infect).

| Primerset      | Forward primer     | Reverse primer       |
|----------------|--------------------|----------------------|
| CLVd-infect           | TGCAGGGTCAGGTGTGAACCAC  | GCCATGCAAAGRAAAAAGAAYGGG   |


The actual primers used in the PCR also contain specific indices that can later be used in the data analysis to discriminate the samples. These indices are used in pairs and are never reused; i.e. each index is used in one combination only, and each combination corresponds to a specific sample.
Below you can see a few examples of primer pairs used for specific samples. Note that the indices can have different lengths.

| Sample | Forward primer                | Reverse primer                     |
|--------|-------------------------------|------------------------------------|
| 1      | **AACAA**TGCAGGGTCAGGTGTGAACCAC    | **GGTCGGTGTA**GCCATGCAAAGRAAAAAGAAYGGG |
| 2      | **TGCATGT**TGCAGGGTCAGGTGTGAACCAC  | **CACCAGCTCC**GCCATGCAAAGRAAAAAGAAYGGG |
| 3      | **ATCCTGCT**TGCAGGGTCAGGTGTGAACCAC | **CCTCGCTAC**GCCATGCAAAGRAAAAAGAAYGGG  |


After the PCR and cleanup, Illumina adapters are ligated to the amplicons. If wanted, these adapters can again contain custom indices for an extra level of indexing, making the number of samples that can be pooled in one sequencing lane even larger.

The advantages of this library prep method are:
- Only 1 PCR instead of a two-step PCR protocol (less PCR bias)
- Relatively short primers due to extra ligation step to ligate Illumina adapters (compared to 1-step PCR protocols)
- No phiX needs to be added to diversify the run because the fragments are ligated in both directions and the indices have different lengths. This leads to higher quality data and more reads.
- Index switching almost impossible since we only retain read pairs in which both indices are found without errors, and the indices are used in only one combination.
- The number of samples which can be pooled is in principle only limited by the number of primer sets you order; and can even be enlarged by making use of an extra index in the adapters of the ligation step.

The disadvantages of this library prep method are:
- A lot of different primers need to be ordered (because of the unique index combination per sample), which makes the start-up cost higher.
- Demultiplexing could not be done using existing tools (see further).

#### Data analysis pipeline

The data analysis pipeline consists of three main steps:
1. Demultiplexing
2. Amplicon processing
3. Exploring the amplicon variants by visualisations in R

The pipeline uses primarily third party software (described below) called through shell scripts.

## Installation requirements

The following software should be installed on your system to run the scripts in this repository (older or newer versions of the software might work as well, but this was not tested). Make sure to check the license of each program to verify whether you are allowed to use it or not.

* Operating system: Ubuntu 18.04
* [sabre 1.000](https://github.com/najoshi/sabre) (demultiplexing tool, should be in $PATH)
* [cutadapt 1.15](https://cutadapt.readthedocs.io/en/stable/guide.html) (primer removal, should be in $PATH)
* [pear 0.9.11](https://cme.h-its.org/exelixis/web/software/pear/) (merging F and R reads, should be in $PATH)
* [vsearch 2.7.1](https://github.com/torognes/vsearch) (quality filtering, should be in $PATH)
* [FASTX toolkit 0.0.14](http://hannonlab.cshl.edu/fastx_toolkit/) (length filtering, should be in $PATH)
* [OBITools 1.2.13](https://pythonhosted.org/OBITools/welcome.html) (sequence variant cleaning and counting, commands should be in $PATH)
* [R](https://www.r-project.org/) and [RStudio](https://rstudio.com/) with the following packages installed: ggplot2, dplyr, phyloseq, RColorBrewer, vegan, reshape2, raster, gplots, tibble


## Demultiplexing script
For the demultiplexing step, at the time we started using these type of libraries (2016), no suitable tools were identified which could handle indices of variable length AND handle the fact that we want to use a combination of indices to identify the sample AND that the fragments could be in two directions (the library is undirectional).
To achieve the demultiplexing, we use the software `sabre` which can handle variable length barcodes, but cannot handle the other issues (barcodes both in F and R reads and undirectional sequencing). The latter issues were covered by running `sabre` multiple times in multiple directions using a shell script.

The script is used as follows:
`demultiplexing.sh -b barcodefile.txt -f forwardreads.fq -r reversereads.fq`

- `-b` : tab delimited file; column 1: name of sample corresponding to a certain  barcode combination; column2: barcode used in primer 1; column3 : barcode used in primer 2
- `-f` : forward reads file to be demultiplexed in fastq format (should be unzipped)
- `-r` : reverse reads file to be demultiplexed in fastq format (should be unzipped)

An example of a barcode file is shown below (tab delimited file).

|         	|         	|            	|
| ---------	| --------- | ----------- |
| Sample1 	| AACAA    	| GGTCGGTGTA 	|
| Sample2 	| TGCATGT  	| CACCAGCTCC 	|
| Sample3 	| ATCCTGCT 	| CCTCGCTAC  	|


## Amplicon processing
The analysis pipeline consists of a single shell script `viroid_amplicon_processing.sh` which does the following:
1. Merging of forward and reverse reads using [pear](https://cme.h-its.org/exelixis/web/software/pear/)
2. Removal of primers using [cutadapt](https://cutadapt.readthedocs.io/en/stable/guide.html)
3. Quality filtering using `fastq_filter`from [vsearch](https://github.com/torognes/vsearch), with a maximum expected error of 0.25
4. Plot the sequence length distribution for all samples using the R script `length_distribution.R`
5. Filter the sequences based on sequence length (min. 300 and max. 350 nt) using [cutadapt](https://cutadapt.readthedocs.io/en/stable/guide.html)
6. Taking the reverse complement of all sequences using `fastx_reverse_complement` from the [FASTX toolkit](http://hannonlab.cshl.edu/fastx_toolkit/)
7. Dereplicate, count and sort the sequences using `obiuniq` and `obisort` from [OBITools](https://pythonhosted.org/OBITools/welcome.html)
8. Rename the sequences to include the sample name using the script `rename_seqs_obitools.pl`, combine all the sequence counts of all samples in a single table and filtering and clean the sequences using `obigrep`, `obisort` and `obitab` from [OBITools](https://pythonhosted.org/OBITools/welcome.html) with as settings for `obigrep`: `-p "count>=mincount"` (in this case we used 1000 as `mincount`)

Before you use the script, you should open it (for example using [Notepad++](https://notepad-plus-plus.org/)) and modify 4 variables on top of the script:
`INPUTDIR` : the absolute path to the directory where the data is (do not use a slash at the end)
`OUTPUTDIR` : the absolute path to the directory where you want to store the output (do not use a slash at the end)
`SCRIPTSDIR` : the absolute path to the directory where the all scripts are
`MINCOUNT` : the minimum number of times a sequence needs to be present in the data (summed over all samples)

For example:

`INPUTDIR=/home/genomics/ahaegeman/Virus/viroid_amplicon_sequencing/Run1/1_Demultiplexing`
`OUTPUTDIR=/home/genomics/ahaegeman/Virus/viroid_amplicon_sequencing/Run1/2_Processing`
`SCRIPTSDIR=/home/genomics/ahaegeman/GitLab/Viroid_amplicon_sequencing/Processing`
`MINCOUNT=1000`


Next, the script is used as follows:
`bash viroid_amplicon_processing.sh`

The output of each step is stored in subdirectories which are made in the folder where the input data is. The final sequence variants with their counts per sample are stored in folder `8_overall_haplotype_counts` and is called `all_derep_mincount1000_sort_final_table.txt`. This tab-delimited text file can be explored further in other programs such as Excel or R. From this table, we visually inspected the sequences and (after alignment) manually removed aspecific sequences (not derived from the viroid). The final table was called `counttable_selectionOh.txt`.

## Exploring the amplicon variants by visualizing in R
The R script `Visualisation.R` was used for the exploration of the resulting variant counts. The script should be read into RStudio. It takes as input the count table (`counttable_selectionOh.txt`) and a metadata file (`metadata.txt`). An example of both files is included in this repository to be able to run the script. The script mainly uses the package [phyloseq](https://joey711.github.io/phyloseq/) to easily filter sequences and/or samples, make barplots, heatmaps, NMDS plots etc.
