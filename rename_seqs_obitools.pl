#!/usr/bin/perl
$|=1;
use strict;
use Bio::SeqIO;
use Data::Dumper;
use warnings;

#dit programma leest een fasta file horend bij alle barcodes in en hernoemt de sequenties zodat ze compatibel zijn met QIIME

################
#set parameters#
################

die  "usage rename_seqs_uparse.pl input.fa staalnaam output.fa" if (scalar @ARGV != 3);

my $inputfile= $ARGV[0];
my $sample = $ARGV[1];
my $outputfile = $ARGV[2];

my ($sequence_object, $id, $desc, $seq,$substring,@line,$verschil,$j);
open (OP,">$outputfile");
$j=0;

#################
#read fasta file#
#################
my $seqio = Bio::SeqIO -> new (
				'-format' => 'fasta',
				'-file' => $inputfile
				);
				
#####################################################
#go through all sequences of fasta file and print id#
#####################################################
while ($sequence_object	 = $seqio -> next_seq) {
	$j++;				#sequentie_nummer
	$id=$sequence_object->id();	#id
	chomp($id);
	@line = split (/\|/,$id);	#splits id naam op pipe symbool, dit omdat je bij de Fungi pipeline werkt met sequenties van ITSx (worden hernoemd en naam bevat pipe symbool, kies enkel het eerste deel)
	#$desc=$sequence_object->desc();	#description
	$seq=$sequence_object->seq();	#sequence
	#$seq=substr($seq,21,);		#verwijder eerste 21 basen (=F-primer)
	#$seq=substr($seq,0,-17);	#verwijder laatste 17 basen (=R-primer)

	#@line = split (/ /,$desc);	#splits id naam op spatie

	print OP ">$line[0]"," sample=",$sample,";","\n";	#print output
	print OP "$seq\n";
	}

